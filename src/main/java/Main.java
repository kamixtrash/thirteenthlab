import impls.ExecutionManagerImpl;
import impls.Task;
import services.Context;
import services.ExecutionManager;

public class Main {
    /**
     * Because I can't create good multithreading tests with JUnit.
     * Guess, I should try TestNG.
     */
    public static void main(String[] args) throws InterruptedException {
        System.out.println("==================FIRST TASK==================");

        Task<String> task = new Task<>(() -> {
            Thread.sleep(500);
            return "foo";
        });
        Runnable someRunnable = () -> System.out.println(task.get());
        for (int i = 0; i < 10; i++) {
            new Thread(someRunnable).start();
        }
        Task<String> task1 = new Task<>(() -> {
            throw new RuntimeException("Some exception text.");
        });
        Runnable exceptionRunnable = () -> {
            System.out.println(task1.get());
        };
        for (int i = 0; i < 10; i++) {
            new Thread(exceptionRunnable).start();
        }
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();
        Thread.sleep(3000);

        System.out.println("==================SECOND TASK==================");
        ExecutionManager manager = new ExecutionManagerImpl();

        Context context = manager.execute(
                () -> System.out.println("Started tasks."),
                () -> {
                    throw new RuntimeException("Exception while doing tasks");
                },
                () -> System.out.println("first task."),
                () -> {
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                    System.out.println("second task.");
                },
                () -> {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                    System.out.println("third task.");
                },
                () -> System.out.println("fourth task."),
                () -> System.out.println("fifth task."),
                () -> System.out.println("sixth task."));

        context.interrupt();
        System.out.println("Interrupted tasks: "
                + context.getInterruptedTasksCount());
        System.out.println("Exceptions was in "
                + context.getFailedTasksCount()
                + " tasks.");

        System.out.println("Completed tasks: "
                + context.getCompletedTaskCount());
        System.out.println("Are all tasks finished? "
                + context.isFinished());
        Thread.sleep(1000);
        System.out.println("Are all tasks finished? "
                + context.isFinished());
        System.out.println("Completed tasks: "
                + context.getCompletedTaskCount());

        System.exit(0);
    }
}
