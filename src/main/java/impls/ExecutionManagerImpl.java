package impls;

import impls.utils.Barrier;
import services.Context;
import services.ExecutionManager;

import java.util.ArrayList;
import java.util.List;

public class ExecutionManagerImpl implements ExecutionManager {
    @Override
    public Context execute(Runnable callback, Runnable... tasks) {
        return new ThreadPool(callback, tasks);
    }

    private class ThreadPool implements Context {
        private final Object lock = new Object();
        private final List<Thread> threadPool;
        private volatile int failedTasksCounter = 0;
        private volatile int interruptedTasksCounter = 0;
        private volatile int completedTasksCounter = 0;

        public ThreadPool(Runnable callback, Runnable... tasks) {
            threadPool = new ArrayList<>();
            Barrier barrier = new Barrier(tasks.length);

            Thread.setDefaultUncaughtExceptionHandler((thread, exception) -> {
                System.out.printf("Uncaught exception in thread %s: %s", thread, exception);
                synchronized (lock) {
                    failedTasksCounter++;
                }
            });

            for (Runnable task : tasks) {
                Thread newThread = new Thread(() -> {
                    try {
                        if (Thread.interrupted()) {
                            synchronized (lock) {
                                interruptedTasksCounter++;
                            }
                            return;
                        }
                        task.run();
                        synchronized (lock) {
                            completedTasksCounter++;
                        }
                    } finally {
                        barrier.countDown();
                    }
                });
                threadPool.add(newThread);
                newThread.start();
            }

            new Thread(() -> {
                barrier.await();
                callback.run();
            }).start();
        }

        private int getAmountOfTasksLeft() {
            return threadPool.size() - interruptedTasksCounter
                    - completedTasksCounter - failedTasksCounter;
        }

        @Override
        public int getCompletedTaskCount() {
            return completedTasksCounter;
        }

        @Override
        public int getFailedTasksCount() {
            return failedTasksCounter;
        }

        @Override
        public int getInterruptedTasksCount() {
            return interruptedTasksCounter;
        }

        @Override
        public void interrupt() {
            threadPool
                    .stream()
                    .filter(thread -> thread.getState() == Thread.State.NEW)
                    .forEach(Thread::interrupt);
        }

        @Override
        public boolean isFinished() {
            return getAmountOfTasksLeft() == 0;
        }
    }
}
