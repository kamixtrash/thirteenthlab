package impls.utils;

/**
 * Simple Barrier that considered as "meeting point"
 * for all threads in group. Program won't move forward
 * while at least on thread in group hasn't reached
 * Barrier.
 * When there is some exception in group, Barrier
 * is considered as broken.
 */
public class Barrier {
    /**
     * Amount of threads in group.
     */
    private final int threads;
    /**
     * Amount of threads ended work.
     */
    private int currentThread;

    public Barrier(int threads) {
        this.threads = threads;
    }

    /**
     * Waits until all threads finish their job.
     */
    public synchronized void await() {
        while (currentThread < threads) {
            try {
                wait();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * Monitoring if all threads finished tasks.
     */
    public synchronized void countDown() {
        if (increment() == threads) {
            notifyAll();
        }
    }

    /**
     * Incrementing amount of threads that completed
     * their work.
     * @return incremented amount of threads.
     */
    private synchronized int increment() {
        return currentThread++;
    }
}
