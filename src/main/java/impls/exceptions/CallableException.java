package impls.exceptions;

public class CallableException extends RuntimeException {
    public CallableException(Throwable cause) {
        super(cause);
    }
}
