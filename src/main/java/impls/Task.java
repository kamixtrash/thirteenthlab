package impls;

import impls.exceptions.CallableException;

import java.util.Objects;
import java.util.concurrent.Callable;

/**
 * Some impls.Task receiving {@link Callable} in constructor.
 * @param <T> generic type of value which method get will return.
 */
public class Task<T> {
    private final Callable<? extends T> callable;
    private volatile T result;

    /**
     * Self-made exception that extends {@link RuntimeException}.
     * @see CallableException
     */
    private CallableException exception;

    public Task(Callable<? extends T> callable) {
        this.callable = callable;
    }

    /**
     * Method returning result of {@link Callable}.
     * If result already stored in {@code result} variable
     * method will return value without entering in "synchronized" area.
     * If method got exception in any {@link Thread} - all of them will
     * throw it.
     * @return result of {@link Callable}
     */
    public T get() {
        if (!Objects.isNull(result)) {
            return result;
        }
        if (!Objects.isNull(exception)) {
            throw exception;
        }
        try {
            synchronized (this) {
                if (!Objects.isNull(result)) {
                    return result;
                }
                result = callable.call();
            }
        } catch (Exception e) {
            throw exception = new CallableException(e);
        }
        return result;
    }
}
