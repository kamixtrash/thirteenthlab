package services;

public interface Context {
    /**
     * @return amount of completed tasks.
     */
    int getCompletedTaskCount();

    /**
     * Amount of {@link Thread} objects that thrown some {@link Exception}.
     * @return amount of failed tasks.
     */
    int getFailedTasksCount();

    /**
     * Amount of {@link Thread} objects that
     * was interrupted with {@code interrupt} method.
     * @return amount of interrupted tasks.
     */
    int getInterruptedTasksCount();

    /**
     * Interrupts tasks, that wasn't started yet.
     * {@see Thread.State.NEW}
     */
    void interrupt();

    /**
     * @return true if 0 tasks left in thread pool.
     */
    boolean isFinished();
}
