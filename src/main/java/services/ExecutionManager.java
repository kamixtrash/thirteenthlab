package services;

public interface ExecutionManager {
    /**
     * Method for executing array of tasks and callback.
     * @param callback executes once after all tasks.
     * @param tasks should be executed in parallel threads.
     * @return {@link Context}
     */
    Context execute(Runnable callback, Runnable... tasks);
}
